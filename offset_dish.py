#!/usr/bin/env python3

#    Copyright (C) 2021  Y. Ritterbusch
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
from scipy.spatial import Delaunay
from matplotlib import pyplot as plt

class OffsetParabolic:
    def __init__(self, width, height, depth):
        """
        Parameters
        ----------
        width : float
            width (smaller dimension) of the offset parabolic dish

        height : float
            height (larger dimension) of the offset parabolic dish

        depth : float
            maximum depth of the offset parabolic dish measured from the rim plane
        """
        self.width = width
        self.height = height
        self.depth = depth

        self.offset_angle = np.arccos(self.width/self.height)
        self.focal_length = self.width**3 / (16 * self.height * self.depth)

        # coordinates of parabola point closest to the origin
        self.y0 = 2 * self.focal_length * np.tan(self.offset_angle) - self.width/2
        self.z0 = self.parabola(0, self.y0)

        # center of projected circular aperture onto the xy-plane
        self.yc = self.y0 + self.width/2
        # z-coordinate at circle center
        self.zc = self.parabola(0, self.yc)

        self.feed_angle = np.arctan((self.focal_length - self.zc)/self.yc)

        # f/D ratio of the equivalent prime focus parabolic dish
        self.fd_ratio = self.focal_length / (self.y0 + self.width)

    def parabola(self, x, y):
        return (x**2 + y**2) / (4 * self.focal_length)

    def surface_points(self, nr=20, nphi=36, freq=2.4e9):
        """
        Compute the x, y and z coordinates of nr * nphi points on an offset
        parabolic dish surface.

        An offset parabolic dish is essentially a subsection of a prime focus
        dish with focal length `a`. When viewed w.r.t. boresight, the offset
        dish's aperture also looks circular, i.e. the projection of the rim
        of the offset dish onto the xy-plane forms a circle whose diameter is equal
        to the smaller dimension (width) of the offset parabolic dish.
        This function evaluates the parabola equation for the prime focus dish
        in a circular xy area corresponding to this projection, yielding the
        points on the surface of the offset parabolic dish.

        More info can be found in [1]

        Paramters
        ---------
        nr : int
            number of points on radial direction

        nphi : int
            number of points in angular direction

        freq : float
            frequency of operation for adaptive meshing

        Returns
        -------
        xyz : array, shape (nr*nphi, 3)
            x, y and z coordinates of the points on the surface of the offset
            parabolic dish

        References
        ----------
        [1] Legon, John A R, "Calculation of the Focal Length of an Offset
            Satellite Dish Antenna", http://www.john-legon.co.uk/offsetdish.htm,
            Visited: 2021-05-15
        """
        # parametrize the projected circle in the xy-plane in radius and angle
        rstop = self.width/2
        A = (3e8 / freq / 8)**2
        dphi = 2 * np.pi / nphi
        r = [0]
        i = 0
        while True:
            i += 1
            ri = np.sqrt(r[i-1]**2 + 4*A/dphi)
            if ri > rstop:
                r.append(rstop)
                break
            else:
                r.append(ri)

        r = np.array(r)
        phi = np.linspace(0, 2*np.pi, nphi, endpoint=False)

        # create cartesian coordinate mesh corresponding to all combinations
        # of r and phi, taking into account the offset from the origin
        x = r[:,np.newaxis] * np.cos(phi[np.newaxis,:])
        y = r[:,np.newaxis] * np.sin(phi[np.newaxis,:]) + self.yc

        # evaluate the parabola equation at the mesh points
        # shift the parabola in the -z-direction such that its focal point
        # lies at the origin of the coordinate system
        z = self.parabola(x, y) - self.focal_length

        xyz = np.stack([x.flatten(), y.flatten(), z.flatten()], axis=-1)
        return xyz

    def trimesh(self, **kwargs):
        """
        Compute a set of triangles that can be used to describe the parabolic
        surface.
        This triangular mesh can be used with antenna modelling software such as
        NEC to represent a parabolic reflector.

        Paramters
        ---------
        for **kwargs see self.surface_points

        Returns
        -------
        points : array, shape (?, 3, 3)
            x, y and z coordinates of three vertices each forming the triangles
        """
        xyz = self.surface_points(**kwargs)
        # use Delaunay tesselation to find 3-tuples of xy pairs that form triangles
        # in the xy-plane
        tri = Delaunay(xyz[:,:2])
        # convert the vertex indices of each simplex into actual xyz coordinate pairs    
        points = np.array([xyz[simplex,:] for simplex in tri.simplices])
        return points

    def nec_deck(self, **kwargs):
        """
        Return the NEC card deck representing this parabolic dish using the
        SP and SC card types to define triangular surface patches

        Paramters
        ---------
        for **kwargs see self.trimesh

        Returns
        -------
        deck : string
            NEC card deck representation of triangular surface patches
        """
        deck = ""
        for (p1, p2, p3) in self.trimesh(**kwargs):
            deck += f"SP{0:3d}{2:5d}{p1[0]:10.4f}{p1[1]:10.4f}{p1[2]:10.4f}{p2[0]:10.4f}{p2[1]:10.4f}{p2[2]:10.4f}\n"
            deck += f"SC{0:3d}{2:5d}{p3[0]:10.4f}{p3[1]:10.4f}{p3[2]:10.4f}\n"
        return deck

class HelicalFeed:
    def __init__(self, center_freq, pitch_angle, nturns,
                circumference_to_wavelength=1,
                groundplane_diameter_to_wavelength=0.94,
                groundplane_cutout_diameter=0,
                groundplane_offset=0,
                wire_diameter=1e-3,
                conductivity=None):
        """
        A helix antenna used as a dish feed

        Parameters
        ----------
        center_freq : float
            design center frequency in Hz

        pitch_angle : float
            helix pitch angle in degrees

        nturns : int
            number of turns

        circumference_to_wavelength : float, optional, Default: 0.94
            ratio of turn circumference to wavelength

        groundplane_diameter_to_wavelength : float, optional, Default: 1.0
            ratio of groundplane diameter to wavelength

        groundplane_cutout_diameter : float, optional, Default: 0
            diameter of circular cutout in the ground plane below the helix

        groundplane_offset : float, optional, Default: 0
            distance between groundplane and start of helix, negative values increase distance

        wire_diameter : float, optional, Deault: 1e-3
            diameter of the wire used for helix and ground plane grid in m

        conductivity : float, optional, Default: None
            conductivity of all wire segments in the helix structure in siemens/m
            if not specified, all wire segments are assumed to be perfect electrical conductors (PEC)
        """
        self.center_freq = center_freq
        self.pitch_angle = np.deg2rad(pitch_angle)
        self.nturns = nturns

        self.circumference = circumference_to_wavelength * self.wavelength
        self.groundplane_diameter = groundplane_diameter_to_wavelength * self.wavelength

        self.groundplane_cutout_diameter_diameter = groundplane_cutout_diameter
        self.groundplane_offset = groundplane_offset
        self.wire_diameter = wire_diameter
        self.conductivity = conductivity

    @property
    def wavelength(self):
        return 3e8 / self.center_freq

    @property
    def diameter(self):
        return self.circumference / np.pi

    @property
    def radius(self):
        return self.diameter / 2

    @property
    def spacing(self):
        return self.circumference * np.tan(self.pitch_angle)
    
    @property
    def length(self):
        return self.spacing * self.nturns

    @property
    def wire_length(self):
        return self.circumference * self.nturns

    @property
    def wire_radius(self):
        return self.wire_diameter / 2

    def nec_deck(self, nphi=18):
        """
        Return the NEC card deck representing this helix feed

        Parameters
        ----------
        nphi : optional, Default: 18
            number of angles for ground plane patches

        Returns
        -------
        deck : string
            NEC card deck representation of helix feed
        """
        deck = ""

        # add a circular reflector made from surtface patches
        rstart = self.groundplane_cutout_diameter_diameter/2
        rstop = self.groundplane_diameter/2
        ncirc = int(np.ceil((rstop - rstart) / self.wavelength * 10))
        r = np.linspace(rstart, rstop, ncirc)
        phi = np.linspace(0, 2*np.pi, nphi, endpoint=False)

        x = r[:, np.newaxis] * np.cos(phi[np.newaxis, :])
        y = r[:, np.newaxis] * np.sin(phi[np.newaxis, :])

        xy = np.stack([x.flatten(), y.flatten()], axis=-1)
        tri = Delaunay(xy)

        simplices = (xy[simplex, :] for simplex in tri.simplices)

        def same_radius(a):
            radius = np.linalg.norm(a, axis=1)
            return np.allclose(radius,radius[0])

        filtered = (simplex for simplex in simplices if not same_radius(simplex))

        for (p1, p2, p3) in filtered:
            deck += f"SP{0:3d}{2:5d}{p1[0]:10.4f}{p1[1]:10.4f}{0:10.4f}{p2[0]:10.4f}{p2[1]:10.4f}{0:10.4f}\n"
            deck += f"SC{0:3d}{2:5d}{p3[0]:10.4f}{p3[1]:10.4f}{0:10.4f}\n"

        deck += f"GM{0:3d}{0:5d}{0:10.4f}{0:10.4f}{0:10.4f}{0:10.4f}{0:10.4f}{self.groundplane_offset:10.4f}{0:10.4f}\n"

        # add the helix
        nseg = int(np.ceil(20 * self.wire_length / self.wavelength))
        deck += f"GH{1:3d}{nseg:5d}{self.spacing:10.4f}{-self.length:10.4f}"+4*f"{self.radius:10.4f}"+f"{self.wire_radius:10.4f}\n"

        # add feed wire between helix start and ground plane
        deck += f"GW{2:3d}{3:5d}{0:10.4f}{self.radius:10.4f}{self.groundplane_offset:10.4f}{0:10.4f}{self.radius:10.4f}{0:10.4f}{self.wire_radius:10.4f}\n"

        return deck

def write_nec_file(fname, freq, dish, helix, rot_angle):
    with open(fname, "w") as f:
        f.write("CM Offset parabolic dish\n")
        f.write(f"CM Offset angle: {np.rad2deg(dish.offset_angle):3.1f} deg\n")
        f.write("CM Helix feed\n")
        f.write(f"CM Center frequency: {helix.center_freq/1e6:10.6f} MHz\n")
        f.write(f"CM Pitch: {np.rad2deg(helix.pitch_angle):2.1f} deg\n")
        f.write(f"CM Number of turns: {helix.nturns:2.1f}\n")
        f.write(f"CM Diameter: {helix.diameter*1e3:4.1f} mm\n")
        f.write(f"CM Length: {he.length*1e3:4.1f} mm\n")
        f.write(f"CM Wire diameter: {he.wire_diameter*1e3:2.1f} mm\n")
        f.write("CE\n")
        f.write(helix.nec_deck())
        f.write(f"GM{0:3d}{0:5d}{rot_angle:10.4f}{0:10.4f}{0:10.4f}{0:10.4f}{0:10.4f}{0:10.4f}{0:10.4f}\n")
        f.write(dish.nec_deck(nr=20,nphi=36,freq=freq))
        f.write(f"GM{0:3d}{0:5d}{90:10.4f}{0:10.4f}{90:10.4f}{0:10.4f}{0:10.4f}{0:10.4f}{0:10.4f}\n")
        f.write(f"GE{0:3d}\nEK\n")
        f.write(f"EX{0:3d}{2:5d}{2:5d}{0:4d}{0:1d}{1:10.4f}{0:10.4f}\n")
        f.write(f"FR{0:3d}{0:5d}{0:5d}{0:5d}{freq/1e6:10.4f}{0:10.4f}\n")

        # define finite conductivity for all wire segments
        if helix.conductivity is not None:
            f.write(f"LD{5:3d}{0:5d}{0:5d}{0:5d}{he.conductivity:10.0f}\n")

if __name__ == "__main__":
    freq = 2.4e9
    cu_resistivity = 16.78e-9

    op = OffsetParabolic(0.73, 0.8, 0.067)
    rot_angle = -1.0*(90 + 67 - np.rad2deg(op.offset_angle))

    for turns in np.arange(3, 6, 0.5):
        he = HelicalFeed(freq, 12.5, turns, groundplane_cutout_diameter=0, groundplane_offset=-10e-3, wire_diameter=1e-3, conductivity=1/cu_resistivity)
        write_nec_file(f"ParabolicDish_Helixfeed_{turns:2.1f}_turns.nec", freq, op, he, rot_angle)
