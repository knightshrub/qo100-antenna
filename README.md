# QO-100 offset parabolic antenna

This program can be used to model an offset parabolic dish antenna fed by a
helix antenna for circular polarisation.

The resulting structure will be output to a [NEC-2](https://en.wikipedia.org/wiki/Numerical_Electromagnetics_Code)
card deck file for further simulation in [4nec2](https://www.qsl.net/4nec2/).
